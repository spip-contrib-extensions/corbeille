# Changelog

## Unreleased

## 4.0.2 - 2024-01-11

### Fixed

- #6 Purger les vieux contextes Ajax

## 4.0.1 - 2023-05-26

### Fixed

- #8 Prendre en compte les Pages uniques

## 4.0.0 - 2023-05-22

### Changed

- Version compatible SPIP4.2
